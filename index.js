// import modul
const express = require('express');
const session = require('express-session');
const expressLayouts = require('express-ejs-layouts');
const cookieParse = require('cookie-parser');
const flash = require('connect-flash');


const { loadCars, addCar, findCars, checkDuplicate, deleteCar, updateCars } = require('./utils/car');
const { body, validationResult, check } = require('express-validator');

// use http://localhost:8000/
const app = express();
const port = 8000;

app.use(express.json()) // for parsing application/ json
app.use(express.urlencoded({ extended: true })) // for parsing application/ x-www-form-urlencoded
app.use(expressLayouts);
app.use(express.static('public')); // serve static files
app.set('view engine', 'ejs');

// configure flash
app.use(cookieParse('secret'));
app.use(
    session({
        cookie: { maxAge: 6000 },
        secret: 'secret',
        resave: true,
        saveUninitialized: true,
    })
);
app.use(flash());

// index page
app.get('/', (req, res) => {
    const cars = loadCars(); //to get list cars
    res.status(200).render('index', {
        layout: 'layouts/main-layout',
        title: 'List Car',
        cars,
        msg: req.flash('msg'),
    });
});

// add page
app.get('/add', (req, res) => {
    res.status(200).render('add', {
        layout: 'layouts/main-layout',
        title: 'Add Car'
    });
});

// update page
app.get('/update/:name', (req, res) => {
    const car = findCars(req.params.name);

    res.status(200).render('update', {
        layout: 'layouts/main-layout',
        title: 'Update Car',
        car
    });
});

// delete
app.get('/delete/:name', (req, res) => {
    const car = findCars(req.params.name);

    // if car not exist
    if (!car) {
        res.status(404);
        res.send('<h1>404</h1>');
    } else {
        deleteCar(req.params.name);
        req.flash('msg', 'Data Berhasil Dihapus');
        res.redirect('/'); // back to get index page
    }
});

// insert process
app.post('/',
    [
        body('name').custom((value) => {
            const duplicate = checkDuplicate(value);
            if (duplicate) {
                throw new Error('name already exist');
            }
            return true;
        }),
        check('price', 'invalid').isNumeric()
    ], (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.render('add', {
                layout: 'layouts/main-layout',
                title: 'Add Car',
                errors: errors.array()
            });
        } else {
            addCar(req.body);
            req.flash('msg', 'Data Berhasil Disimpan');
            res.redirect('/');
        }
    })

// update process
app.post('/update',
    [
        body('name').custom((value, { req }) => {
            const duplicate = checkDuplicate(value);
            if (value !== req.body.oldName && duplicate) {
                throw new Error('name already exist');
            }
            return true;
        }),
        check('price', 'invalid').isNumeric()
    ],
    (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.render('update', {
                layout: 'layouts/main-layout',
                title: 'Update Car',
                errors: errors.array(),
                car: req.body,
            });
        } else {
            updateCars(req.body);
            // send flash error
            req.flash('msg', 'Data Berhasil Disimpan');
            res.redirect('/');
        }
    });


// if the request page is not recognized
app.use((req, res) => {
    res.status(400).send('404 not found');
})

// to run port 8000
app.listen(port, () => {
    console.log(`App listening on http://localhost:${port}`);
});

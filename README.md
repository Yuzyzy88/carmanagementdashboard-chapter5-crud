<p align="center">
<img src="./public/images/index1.png" width="190">
<img src="./public/images/index.png" width="150">
<img src="./public/images/add.png" width="150">
</p>

# Challenge FSW Chapter 5 - CRUD
## About

This web design uses View Engine in Node.js to fulfill the needs of mobile data management. This web can create, update and delete car data which will be stored in cars.json data. Made using:

- [Bootstrap v5.0](https://getbootstrap.com/).
- [Express.js 4.17.3](http://expressjs.com/).
- [Template engine ejs](https://github.com/mde/ejs).
- [npm v8.5.0.](https://docs.npmjs.com/cli/v6/commands/npm-install).
- [Node.js v16.14.2.](https://nodejs.org/en/).

What to install :
- npm install

Run command to start :
- nodemon index.js

## Info

Design using basic templates from [SafnaPrasetiono](https://github.com/SafnaPrasetiono/Dashboard-Part1) and changed according to the design [Binar Academy](https://www.figma.com/file/H6xTtBW9Kzlf09nYnitvbH/BCR---Car-Management-Dashboard?node-id=18343%3A5831)
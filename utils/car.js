const fs = require('fs');

// function to get all data in cars.json
const loadCars = () => {
    const fileBuffer = fs.readFileSync('data/cars.json', 'utf-8');
    const cars = JSON.parse(fileBuffer);
    return cars;
}

// funcion find data by name 
const findCars = (name) => {
    const cars = loadCars();
    const car = cars.find((car) => car.name.toLowerCase() === name.toLowerCase());
    return car;
}

// overwrite cars.json file with new data
const saveCars = (cars) => { // cars = object
    fs.writeFileSync('data/cars.json', JSON.stringify(cars));
}

// function add car
const addCar = (car) => {
    const cars = loadCars();
    cars.push(car);
    saveCars(cars);
}

// check duplicate name
const checkDuplicate = (name) => {
    const cars = loadCars();
    return cars.find((car) => car.name === name);
}

// delete car
const deleteCar = (name) => {
    const cars = loadCars();
    const filteredCars = cars.filter((cars) => cars.name !== name);
    saveCars(filteredCars);
}

// update car
const updateCars = (newCar) => {
    const cars = loadCars();
    const filteredCars = cars.filter((car) => car.name !== newCar.oldName);
    delete newCar.oldName; // hilangkan car lama yang namanya sama dengan oldName
    filteredCars.push(newCar);
    saveCars(filteredCars);
}

// export function loadCars
module.exports = { loadCars, findCars, checkDuplicate, addCar, deleteCar, updateCars };